// Criação de tela inicial com DOM
const main = document.querySelector('main');
const telainicial = document.querySelector('.tela-inicial');
const imageBoy = document.querySelector('#profile-boy');
const imageGirl = document.querySelector('#profile-girl');


//Criação da tela de jogo com DOM
const telajogo = document.createElement('section');
const divPlayer = document.createElement('div');
divPlayer.className = "divPlayer";
const papelPlayer = document.createElement('img');
papelPlayer.src = "./img/papel.png";
papelPlayer.className = "imgPlayer"
const pedraPlayer = document.createElement('img');
pedraPlayer.src = "./img/pedra.png";
pedraPlayer.className = "imgPlayer";
const tesouraPlayer = document.createElement('img');
tesouraPlayer.src = "./img/tesoura.png";
tesouraPlayer.className = "imgPlayer";
const divMachine = document.createElement('div');
divMachine.className = "divMachine";
const papelMachine = document.createElement('img');
papelMachine.src = "./img/papel.png";
papelMachine.className = "imgMachine";
const pedraMachine = document.createElement('img');
pedraMachine.src = "./img/pedra.png";
pedraMachine.className = "imgMachine";
const tesouraMachine = document.createElement('img');
tesouraMachine.src = "./img/tesoura.png";
tesouraMachine.className = "imgMachine";


//Criação da tela de vitória com DOM
const telafinal = document.createElement('section');
telafinal.className = ".tela-final";
const divmessage = document.createElement('div');
const divPlacarP = document.createElement('div');
const divPlacarM = document.createElement('div');
divmessage.className = "info";
divPlacarP.className = "info";
divPlacarM.className = "info";

// Função de inserção de elementos HTML
function telaJogo(){
    main.appendChild(telajogo);
    telajogo.appendChild(divPlayer);
    divPlayer.appendChild(papelPlayer);
    divPlayer.appendChild(pedraPlayer);
    divPlayer.appendChild(tesouraPlayer);
    telajogo.appendChild(divMachine);
    divMachine.appendChild(papelMachine);
    divMachine.appendChild(pedraMachine);
    divMachine.appendChild(tesouraMachine);
}

// Função de tela do vencedor
function telaFinal(win){  
    divmessage.innerHTML = `Vencedor: ${win}`;
    telajogo.remove('section');       
    setTimeout( () => {
        location.reload()
    }, 3000);
  }
