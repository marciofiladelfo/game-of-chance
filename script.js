//Evento de click da seleção do player
imageBoy.addEventListener('click', profilemenino);
imageGirl.addEventListener('click', profilemenina);

//Padronização para meninos
function profilemenino() {
  telainicial.remove('tela-inicial');
  document.body.style.background = "linear-gradient(to right, #000046, #1cb5e0)";
  telaJogo();
}

//Padronização para meninas
function profilemenina() {
  telainicial.remove('tela-inicial');
  document.body.style.background = "linear-gradient(to right, #642b73, #c6426e)";
  telaJogo();
}

//Objeto gerador de números aleatórios
const random = {
  state: 0,
  flip: function () {
    return this.state = Math.floor((Math.random() * 3));
  },
  toImageMachine: function () {
    if (this.state === 0) {
      pedraMachine.style.display = "none";
      tesouraMachine.style.display = "none";
      papelMachine.style.display = "block"
    } else if (this.state === 1) {
      papelMachine.style.display = "none";
      tesouraMachine.style.display = "none";
      pedraMachine.style.display = "block"
    } else {
      pedraMachine.style.display = "none";
      papelMachine.style.display = "none";
      tesouraMachine.style.display = "block"
    }
  }
};

//Eventos de click gerando valor na escolha
papelPlayer.onclick = () => { move(0); };
pedraPlayer.onclick = () => { move(1); };
tesouraPlayer.onclick = () => { move(2); };


let pointsPlayer = 0;
let pointsMachine = 0;

// Função de jogada
function move(escolha) {
  player = escolha;
  machine = random.flip();
  random.toImageMachine();
  validation(player, machine);
}

// Função da lógica de validação
function validation(player, machine) {
  if (player == machine) {
    divmessage.innerHTML = "Empatou, tente denovo!";
    message();
  }
  else if (player == 0 && machine == 1) {
    divmessage.innerHTML = `Ponto para você`;
    pointsPlayer++;
    message();
  }
  else if (player == 0 && machine == 2) {
    divmessage.innerHTML = "Ponto para a Máquina";
    pointsMachine++;
    message();
  }
  else if (player == 1 && machine == 0) {
    divmessage.innerHTML = "Ponto para a Máquina";
    pointsMachine++;
    message();
  }
  else if (player == 1 && machine == 2) {
    divmessage.innerHTML = `Ponto para você`;
    pointsPlayer++;
    message();
  }
  else if (player == 2 && machine == 0) {
    divmessage.innerHTML = `Ponto para você`;
    pointsPlayer++;
    message();
  }
  else if (player == 2 && machine == 1) {
    divmessage.innerHTML = "Ponto para a Máquina";
    pointsMachine++;
    message();
  }
  winner();
}

// Função que declara o vencedor com 10 pontos
function winner(){
  if(pointsMachine >= 10){
    let win = "Máquina";
    telaFinal(win);
  }else if(pointsPlayer >= 10){
    let win = "Você!!";
    telaFinal(win);
  }
}

// Função que exibe mensagens
function message() {
  main.appendChild(telafinal);
  divPlacarP.innerHTML = pointsPlayer;
  divPlacarM.innerHTML = pointsMachine;
  telafinal.appendChild(divPlacarP);
  telafinal.appendChild(divmessage);
  telafinal.appendChild(divPlacarM);
}
